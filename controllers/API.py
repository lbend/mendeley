import requests

def get_datasets():
    print 'obteniendo el dataset para el query: %s' % request.vars.query
    cabeceras = {'Authorization': 'Bearer %s' % request.vars.token}
    parametros = {'fields': 'results.*',
                  'limit': 500,
                  'type': 'document',
                  'sort': 'popularity',
                  'query': request.vars.query}
    resultado = requests.get('https://api.mendeley.com/datasets', headers=cabeceras, params=parametros)
    return 'resultado code: %i. resultado text: %s' % (resultado.status_code, resultado.text)


def get_documents():
    cabeceras = {'Authorization': 'Bearer %s' % request.vars.token}
    parametros = {'include_trashed': True,
                  'view': 'all',
                  'limit': 500}
    resultado = requests.get('https://api.mendeley.com/documents', headers=cabeceras, params=parametros)
    return 'resultado code: %i. resultado text: %s' % (resultado.status_code, resultado.text)