# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------
from applications.RecomendadorExpertosMendeley.modules.MendeleyAPI import MendeleyAPI
from applications.RecomendadorExpertosMendeley.modules.propiedades import redirection_uri, application_secret, application_id


def get_autorizacion_actual():
    autorizaciones = db(db.autorizacion.usuario == auth.user_id).select(db.autorizacion.ALL)
    autorizacion_cargada = None
    for autorizacion in autorizaciones:
        if autorizacion.autorizado_por_usuario is True:
            autorizacion_cargada = {'autorizado_por_usuario': True,
                                    'access_token': autorizacion.access_token,
                                    'refresh_token': autorizacion.refresh_token}

    return autorizacion_cargada


@auth.requires_login()
def solicitar_autorizacion_mendeley():
    import random

    state = random.random()
    session.mendeley_authorization_state = round(state, 6)
    redirect('https://api.mendeley.com/oauth/authorize?'
             'client_id=%i&'
             'redirect_uri=%s&'
             'response_type=code&'
             'scope=all&'
             'state=%f' % (application_id,
                           redirection_uri,
                           state))


@auth.requires_login()
def crear_task(id_topico, fecha_publicacion):
    segundos_por_dia = 86400
    tarea = scheduler.queue_task('task_procesar_topico',
                                 pvars=dict(id_topico=id_topico),
                                 start_time=fecha_publicacion,
                                 timeout=segundos_por_dia)

    db(db.topicos.id == id_topico).update(id_tarea=tarea.id)


@auth.requires_login()
def eliminar_task(id_topico):
    tarea_topico = db(db.topicos.id == id_topico).select(db.topicos.id_tarea).first()
    scheduler.stop_task(tarea_topico.id_tarea)


@auth.requires_login()
@request.restful()
def api():
    response.view = 'generic.json'
    def POST(type, **vars):
        if type == 'CREATE':
            vars['usuario'] = auth.user_id
            nuevo_topico = db.topicos.validate_and_insert(**vars)
            crear_task(nuevo_topico.id, vars['fecha_publicacion'])
            return {'estado': 'OK', 'id_topico': nuevo_topico.id}
        elif type == 'DELETE':
            eliminar_task(vars['id'])
            db(db.topicos.id == vars['id']).delete()
            return {'estado': 'OK'}
        else:
            return {'estado': 'ERROR no existe el endpoint %s' % type}
    return locals()


@auth.requires_login()
def on_create_registro(form):
    crear_task(form.vars.id, form.vars.fecha_publicacion)


@auth.requires_login()
def on_delete_registro(table, id):
    eliminar_task(id)


@auth.requires_login()
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    solicitar_autorizacion = False
    autorizacion_actual = get_autorizacion_actual()
    if autorizacion_actual is not None:
        if autorizacion_actual['autorizado_por_usuario'] is False:
            solicitar_autorizacion = True
    else:
        solicitar_autorizacion = True

    if solicitar_autorizacion:
        solicitar_autorizacion_mendeley()
    else:
        form_registro = SQLFORM.grid(db.topicos,
                                     oncreate=on_create_registro,
                                     ondelete=on_delete_registro,
                                     details=False,
                                     csv=False)

        return dict(form_registro=form_registro)


def autorizacion():
    if session.mendeley_authorization_state == float(request.vars.state):
        mendeleyAPI = MendeleyAPI(auth.user_id)
        data = {'grant_type': 'authorization_code',
                'code': request.vars.code}
        autorizado = mendeleyAPI.autorizar(data_especifica=data)
        if autorizado:
            redirect(URL('index'))
    else:
        return 'Ataque detectado'
    return dict()


@auth.requires(auth.has_membership('administradores'), requires_login=True)
def administrar_cuentas():
    grilla_usuarios = SQLFORM.grid(db.auth_user, csv=False)
    return locals()


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


