# -*- coding: utf-8 -*-
from applications.RecomendadorExpertosMendeley.modules import propiedades
from gluon.tools import Auth
from gluon import current


db = DAL(propiedades.db_uri)
auth = Auth(db)
auth.define_tables(username=False, signature=True)
# auth.settings.actions_disabled.append('register')
auth.settings.actions_disabled.append('request_reset_password')
auth.settings.allow_basic_login = True

db.define_table('autorizacion',
                Field('autorizado_por_usuario', 'boolean', readable=False, writable=False, default=False),
                Field('access_token', 'text', readable=False, writable=False, default=''),
                Field('refresh_token', 'text', readable=False, writable=False, default=''),
                Field('usuario', 'reference auth_user', default=auth.user_id))

db.define_table('topicos',
                Field('nombre_topico', 'string', required=True),
                Field('busquedas', 'text', required=True),
                Field('fecha_publicacion', 'datetime', required=True,
                      writable=True if request.vars.id is None else False),
                Field('id_tarea', 'integer', readable=False, writable=False),
                Field('usuario', 'reference auth_user', default=auth.user_id, readable=False, writable=False))

db.topicos.id.readable = db.topicos.id.writable = False

current.db = db

response.logo = None
response.menu = None