from applications.RecomendadorExpertosMendeley.modules.SchedulerTasksTopico import SchedulerTasksTopico
from gluon.scheduler import Scheduler
import logging
from logging.handlers import RotatingFileHandler
import time


def configurar_logger():
    logger = logging.getLogger('RecomendadorExpertosMendeley')
    logger.setLevel(logging.INFO)
    handler = RotatingFileHandler('./applications/RecomendadorExpertosMendeley/logs/scheduler_worker.log', maxBytes=500000, backupCount=5)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


def task_procesar_topico(id_topico):
    import sys
    sys.setrecursionlimit(900000)

    logger = configurar_logger()
    # Debo recordar que no debo retornar ningun string (u otro valor) muy grande ni nada. Stdout debe
    # permanecer lo mas limpio posible
    topico = db(db.topicos.id == id_topico).select(db.topicos.nombre_topico,
                                                   db.topicos.busquedas,
                                                   db.topicos.usuario).first()

    scheduler_tasks_topico = SchedulerTasksTopico(topico.nombre_topico,
                                                  topico.busquedas.split('\n'),
                                                  topico.usuario)
    status_twitter, status_mendeley = scheduler_tasks_topico.procesar_topico()
    logger.debug('Status mendeley: %s' % status_mendeley)
    logger.info('Status twitter: %s' % status_twitter)

    if status_mendeley != '':
        logger.debug('Publicando status mendeley')
        scheduler_tasks_topico.publicar_expertos(status_mendeley)
        quince_minutos = 60 * 15
        logger.debug('durmiendo por 15 minutos')
        time.sleep(quince_minutos)
    if status_twitter != '':
        logger.info('publicando status twitter')
        scheduler_tasks_topico.publicar_expertos(status_twitter)
    logger.info('FIN')


db_scheduler = DAL(propiedades.db_scheduler)
scheduler = Scheduler(db_scheduler, tasks=dict(task_procesar_topico=task_procesar_topico))
