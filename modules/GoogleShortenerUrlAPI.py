import requests
import json
import logging

class GoogleShortenerUrlAPI:

    def __init__(self):
        '''Implementacion del api de google shortener url (https://goo.gl/) para lo cual segui
        la guia (https://developers.google.com/url-shortener/v1/getting_started)'''
        self.API_KEY = 'AIzaSyDcnegDk46-X9hz0pgvW0nuvdBAiPNQdqo'
        self.logger = logging.getLogger('RecomendadorExpertosMendeley.%s' % __name__)

    def get_url_corta(self, url):
        cabeceras = {'Content-Type': 'application/json'}
        payload = {'longUrl': url}
        parametros = {'key': self.API_KEY}

        resultado = requests.post('https://www.googleapis.com/urlshortener/v1/url',
                                  headers=cabeceras,
                                  data=json.dumps(payload),
                                  params=parametros)

        jsonResponse = json.loads(resultado.text)
        self.logger.debug('URL %s recortada: %s' % (url, jsonResponse['id']))
        return jsonResponse['id']
