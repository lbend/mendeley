from applications.RecomendadorExpertosMendeley.modules.propiedades import redirection_uri, application_id, application_secret
from gluon import current
import requests
import logging

class EmptyResponse:

    def __init__(self):
        self.status_code = 200
        self.text = '[]'

class MendeleyAPI:

    def __init__(self, user_id):
        self.logger = logging.getLogger('RecomendadorExpertosMendeley.%s' % __name__)
        self.user_id = user_id

    def autorizar(self, data_especifica):
        from requests.auth import HTTPBasicAuth
        import json

        cabeceras = {'Content-Type': 'application/x-www-form-urlencoded'}
        data = {'redirect_uri': redirection_uri}
        data.update(data_especifica)

        response = requests.post('https://api.mendeley.com/oauth/token',
                                 auth=HTTPBasicAuth(application_id, application_secret),
                                 data=data,
                                 headers=cabeceras)
        if response.status_code == 200:
            jsonResponse = json.loads(response.text)
            db = current.db
            db.autorizacion.update_or_insert(db.autorizacion.usuario == self.user_id,
                                             autorizado_por_usuario=True,
                                             access_token=jsonResponse['access_token'],
                                             refresh_token=jsonResponse['refresh_token'])
            return True
        else:
            self.logger.error('Error al intentar realizar la autorizacion')
            self.logger.error('data: %s' % data)
            self.logger.error('response status_code: %i' % response.status_code)
            self.logger.error('response text: %s' % response.text)

        return False

    def get_access_token(self):
        db = current.db
        autorizacion = db(db.autorizacion.usuario == self.user_id).select(db.autorizacion.access_token).first()
        if autorizacion is not None:
            return autorizacion.access_token

        return ''

    def get_refresh_token(self):
        db = current.db
        autorizacion = db(db.autorizacion.usuario == self.user_id).select(db.autorizacion.refresh_token).first()
        if autorizacion is not None:
            return autorizacion.refresh_token

        return ''

    def refrescar_token(self):
        data = {'grant_type': 'refresh_token',
                'refresh_token': self.get_refresh_token()}
        autorizado = self.autorizar(data_especifica=data)
        if autorizado:
            return True
        return False

    def ejecutar_peticion(self, endpoint, cabeceras=None, parametros=None, numero_intento=1):
        from requests.exceptions import ConnectionError
        from time import sleep

        max_intentos = 5
        try:
            if numero_intento <= max_intentos:
                resultado = requests.get(endpoint,
                                         headers=cabeceras,
                                         params=parametros)

                if resultado.status_code == 401:
                    if 'token' in resultado.text.lower() and 'expired' in resultado.text.lower():
                        if self.refrescar_token() is True:
                            cabeceras['Authorization'] = 'Bearer %s' % self.get_access_token()
                            self.logger.debug('Exito en el refresco del token, lanzo ConnectionError para reutilizar '
                                              'el codigo recursivo definido en el handler de esa excepcion')
                            numero_intento -= 1
                            raise ConnectionError
                        else:
                            self.logger.error('No se pudo refrescar el token')

                return resultado
            return EmptyResponse()
        except ConnectionError:
            sleep(numero_intento)
            numero_intento += 1
            return self.ejecutar_peticion(endpoint, cabeceras, parametros, numero_intento)

    def get_catalog_search(self, query):
        cabeceras = {'Authorization': 'Bearer %s' % self.get_access_token(),
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        parametros = {'limit': 100,
                      'view': 'all',
                      'open_access': True,
                      'query': query}

        resultado = self.ejecutar_peticion('https://api.mendeley.com/search/catalog',
                                           cabeceras,
                                           parametros)
        if resultado.status_code == 200:
            return resultado.text

        return '[]'

    def get_profile_by_id(self, id_profile):
        cabeceras = {'Authorization': 'Bearer %s' % self.get_access_token(),
                     'Accept': 'application/vnd.mendeley-profiles.1+json'}

        resultado = self.ejecutar_peticion('https://api.mendeley.com/profiles/%s' % str(id_profile),
                                           cabeceras)
        if resultado.status_code == 200:
            return resultado.text

        return '[]'

    def search_profiles(self, author_name):
        cabeceras = {'Authorization': 'Bearer %s' % self.get_access_token(),
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        parametros = {'query': author_name}

        resultado = self.ejecutar_peticion('https://api.mendeley.com/search/profiles',
                                           cabeceras,
                                           parametros)
        if resultado.status_code == 200:
            return resultado.text

        return '[]'

    def get_followers(self, followed_id):
        cabeceras = {'Authorization': 'Bearer %s' % self.get_access_token(),
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        parametros = {'followed': followed_id}

        resultado = self.ejecutar_peticion('https://api.mendeley.com/followers',
                                           cabeceras,
                                           parametros)
        if resultado.status_code == 200:
            return resultado.text

        return '[]'

    def get_followees(self, follower_id):
        cabeceras = {'Authorization': 'Bearer %s' % self.get_access_token(),
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        parametros = {'follower': follower_id}

        resultado = self.ejecutar_peticion('https://api.mendeley.com/followers',
                                           cabeceras,
                                           parametros)
        if resultado.status_code == 200:
            return resultado.text

        return '[]'
