from applications.RecomendadorExpertosMendeley.modules.GoogleShortenerUrlAPI import GoogleShortenerUrlAPI
from applications.RecomendadorExpertosMendeley.modules.MendeleyAPI import MendeleyAPI
from applications.RecomendadorExpertosMendeley.modules.TwitterAPI import TwitterAPI
from applications.RecomendadorExpertosMendeley.modules.nlp import Nlp
import json
import networkx
import logging
from applications.RecomendadorExpertosMendeley.modules.propiedades import stop_umbral_numero_investigadores, \
    porcentaje_documentos_a_recorrer, numero_cuentas_expertos_finales, incluir_cuentas_mendeley


class SchedulerTasksTopico:

    def __init__(self, nombre_topico, lista_querys_busqueda, user_id):
        self.mendeleyAPI = MendeleyAPI(user_id)
        self.twitterAPI = TwitterAPI()
        self.nlp = Nlp()
        self.grafo_investigadores = networkx.DiGraph()
        self.histograma_topico = dict()
        self.nombre_topico = nombre_topico
        self.lista_querys_busqueda = lista_querys_busqueda
        self.logger = logging.getLogger('RecomendadorExpertosMendeley.%s' % __name__)

    def actualizar_grafo_investigadores(self, autor_id):
        if len(self.grafo_investigadores.nodes()) > stop_umbral_numero_investigadores:
            self.logger.debug('grafo investigadores mayor a %i: %i' % (stop_umbral_numero_investigadores,
                                                                       len(self.grafo_investigadores.nodes())))
            return

        response = self.mendeleyAPI.get_followees(autor_id)
        response = json.loads(response)
        for par_autores in response:
            if par_autores['status'] == 'following':
                if self.grafo_investigadores.has_edge(par_autores['follower_id'],
                                                      par_autores['followed_id']) is False:
                    self.grafo_investigadores.add_edge(par_autores['follower_id'],
                                                       par_autores['followed_id'])

                    follower_node = 0
                    current_follwer_edges = [edge[follower_node] for edge in self.grafo_investigadores.edges()]
                    if par_autores['followed_id'] not in current_follwer_edges:
                        self.logger.debug('len nodes: %i' % len(self.grafo_investigadores.nodes()))
                        self.actualizar_grafo_investigadores(par_autores['followed_id'])

        if len(response) == 0:
            # Actualizo el grafo con aquellos autores que no siguen a nadie
            if self.grafo_investigadores.has_node(autor_id) is False:
                self.grafo_investigadores.add_node(autor_id)
                self.logger.debug('nodo anadido: %s' % autor_id)

    def procesar_autor(self, autor):
        try:
            nombre_autor = '%s %s' % (autor['first_name'], autor['last_name'])
        except KeyError as e:
            if 'first_name' in autor.keys():
                nombre_autor = autor['first_name']
            elif 'last_name' in autor.keys():
                nombre_autor = autor['last_name']
                if autor['last_name'] == 'others':
                    return
            else:
                raise AttributeError('%s. %s. %s' % ('El autor no tiene nombre ni representa "others"',
                                                     e.message,
                                                     str(autor)))
        autores_response = self.mendeleyAPI.search_profiles(nombre_autor)
        autores_response = json.loads(autores_response)
        for autor_response in autores_response:
            coincidencia = False
            if 'first_name' in autor_response.keys() and 'first_name' in autor.keys():
                if autor_response['first_name'].lower() == autor['first_name'].lower():
                    coincidencia = True
            if 'last_name' in autor_response.keys() and 'last_name' in autor.keys():
                if autor_response['last_name'].lower() in autor['last_name'].lower():
                    coincidencia = True
                else:
                    coincidencia = False

            if coincidencia:
                autor_profile_id = autor_response['id']
                self.actualizar_grafo_investigadores(autor_profile_id)

    def recorrer_documentos(self, documentos):
        total_documentos = len(documentos)
        max_documentos_a_recorrer = (porcentaje_documentos_a_recorrer * total_documentos) / 100
        if total_documentos > 0 and max_documentos_a_recorrer == 0:
            max_documentos_a_recorrer = 1
        self.logger.debug('Max documentos a recorrer: %i' % max_documentos_a_recorrer)
        for documento in documentos[:max_documentos_a_recorrer]:
            if 'authors' in documento.keys():
                for autor in documento['authors']:
                    self.procesar_autor(autor)

    def actualizar_histograma_documentos(self, documentos):
        for documento in documentos:
            if 'abstract' in documento.keys():
                self.nlp.actualizar_histograma(self.histograma_topico, documento['abstract'])

    def get_histograma_tweets(self, tweets):
        histograma_tweets = dict()

        for tweet in tweets:
            self.nlp.actualizar_histograma(histograma_tweets, tweet)

        return histograma_tweets

    def get_ids_expertos_finales(self, grafo_investigadores):
        page_rank = networkx.pagerank(grafo_investigadores)
        return sorted(page_rank, key=page_rank.get, reverse=True)

    def get_cuentas_expertos_finales(self, ids_expertos_finales):
        cuentas_expertos_finales = dict()
        contador_cuentas_expertos_agregadas = 0
        for id_experto in ids_expertos_finales:
            if contador_cuentas_expertos_agregadas >= numero_cuentas_expertos_finales:
                break
            experto_data_raw = self.mendeleyAPI.get_profile_by_id(id_experto)
            experto_data = json.loads(experto_data_raw)
            if len(experto_data) > 0:
                cuentas_expertos_finales[id_experto] = list()
                cuentas_expertos_finales[id_experto].append(experto_data['link'])
                nombre_experto = '%s %s' % (experto_data['first_name'], experto_data['last_name'])
                cuentas_expertos = self.twitterAPI.get_user_accounts_by_name(nombre_experto)
                for cuenta_experto in cuentas_expertos:
                    self.logger.debug('Analizando tweets de la cuenta @%s' % cuenta_experto)
                    tweets_recientes = self.twitterAPI.get_last_tweets(cuenta_experto)
                    histograma_tweets_cuenta = self.get_histograma_tweets(tweets_recientes)
                    if self.nlp.histogramas_son_similares(histograma_tweets_cuenta, self.histograma_topico):
                        self.logger.debug('Experto @%s encontrado en base al nombre de experto %s.\n%s' % (cuenta_experto,
                                                                                                           nombre_experto,
                                                                                                           experto_data_raw))
                        cuentas_expertos_finales[id_experto].append(cuenta_experto)
                        contador_cuentas_expertos_agregadas += 1
                        break

        return cuentas_expertos_finales

    def construir_status_twitter(self, cuentas_expertos_finales):
        url_shortener = GoogleShortenerUrlAPI()

        if len(cuentas_expertos_finales.keys()) > 0:
            string_mencion_cuentas_twitter = ''
            string_mencion_cuentas_mendeley = ''
            status_twitter = ''
            status_mendeley = ''
            for cuenta in cuentas_expertos_finales:
                cuenta_mendeley = 0
                cuenta_twitter = 1
                if len(cuentas_expertos_finales[cuenta]) == 2:
                    string_mencion_cuentas_twitter += '@%s, ' % cuentas_expertos_finales[cuenta][cuenta_twitter]
                elif len(cuentas_expertos_finales[cuenta]) == 1 and incluir_cuentas_mendeley is True:
                    string_mencion_cuentas_mendeley += '%s, ' % url_shortener.get_url_corta(
                        cuentas_expertos_finales[cuenta][cuenta_mendeley])

            if string_mencion_cuentas_twitter != '':
                status_twitter = '#' + self.nombre_topico.replace(' ', '_').decode('utf-8') + ': En Twitter te recomendamos seguir a los usuarios ' + \
                                 string_mencion_cuentas_twitter
                self.logger.debug('Finalizando con status de twitter: %s' % status_twitter)
            if string_mencion_cuentas_mendeley != '':
                status_mendeley = '#' + self.nombre_topico.replace(' ', '_').decode('utf-8') + ': En Mendeley te recomendamos seguir a los usuarios ' + \
                                  string_mencion_cuentas_mendeley

            return status_twitter, status_mendeley
        return '', ''

    def procesar_topico(self):
        for busqueda in self.lista_querys_busqueda:
            documentos = self.mendeleyAPI.get_catalog_search(busqueda.strip())
            documentos = json.loads(documentos)
            self.recorrer_documentos(documentos)
            self.actualizar_histograma_documentos(documentos)

        cuentas_expertos_finales = self.get_cuentas_expertos_finales(self.get_ids_expertos_finales(self.grafo_investigadores))

        return self.construir_status_twitter(cuentas_expertos_finales)

    def procesar_expertos_a_publicar(self, status):
        max_caracteres_tweet = 140
        contador_caracteres = 0
        perfiles_expertos = status.split(', ')
        perfiles_finales_expertos = list()
        for perfil_experto in perfiles_expertos:
            if (len(perfil_experto) + contador_caracteres) <= max_caracteres_tweet:
                perfiles_finales_expertos.append(perfil_experto)
                contador_caracteres += len(perfil_experto)
            else:
                break
        status = ' '.join(perfiles_finales_expertos)

        return status

    def publicar_expertos(self, status):
        status = self.procesar_expertos_a_publicar(status)
        self.twitterAPI.update_status(status)
