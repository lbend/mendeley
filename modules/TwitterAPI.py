from twython import Twython
from twython.exceptions import TwythonAuthError, TwythonRateLimitError, TwythonError
import re
import time
import logging


class TwitterAPI:

    def __init__(self):
        # Si se desea utilizar otras credenciales ya sea por motivos de migracion de cuenta u otros
        # se pueden seguir los pasos descritos en el punto 4.2 (Obtencion de credenciales de Twitter)
        # de la siguiente guia: https://goo.gl/Sf9igk
        API_KEY = 'P4CtLcG3Krnr7B1IXcuQmRdSZ'
        APP_SECRET = 'Vzu3IJJONs5pElfyJAmZnqjRdL70uQZ8SvfmKDaqD45S9x3nR9'
        OAUTH_TOKEN = '821380569351262209-rYqqIrTqvOoVlV56GSzdZaI8r7zVgJm'
        OAUTH_TOKEN_SECRET = 'r95GLJOijKpanP4EQkobo4vxGuBb0tTvCcZgVn1HaLoev'

        self.twitterAPI = Twython(API_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)

        self.logger = logging.getLogger('RecomendadorExpertosMendeley.%s' % __name__)

    def pause_requests(self):
        fifteen_minutes_in_seconds = 15 * 60
        pause_time = fifteen_minutes_in_seconds + 1
        self.logger.debug('control rate limit, sleeping for %i seconds' % pause_time)
        time.sleep(pause_time)
        self.logger.debug('control rate limit, waking up')

    def get_user_accounts_by_name(self, name, intento=1):
        try:
            if intento < 5:
                response = self.twitterAPI.search_users(q=name.strip(),
                                                        count=20)
                return [result['screen_name'].strip() for result in response]
            return []
        except TwythonAuthError:
            return []
        except TwythonError as te:
            too_many_requests = 429
            if te.error_code != too_many_requests:
                self.logger.error('TwythonError en get_user_accounts_by_name:')
                self.logger.error(te)
                self.logger.error(te.error_code)
            if intento < 5:
                self.pause_requests()
                intento += 1
                return self.get_user_accounts_by_name(name, intento)
            return []

    def remove_retweet_from_tweet(self, tweet):
        retweet = re.compile(r'rt\s')
        tweet = retweet.sub('', tweet)
        return tweet

    def remove_remaining_urls(self, tweet):
        url = re.compile(r'http[^\s]*')
        tweet = url.sub('', tweet)
        return tweet

    def get_user_timeline(self, user_screen_name, intento=1):
        try:
            if intento < 5:
                response = self.twitterAPI.get_user_timeline(screen_name=user_screen_name,
                                                             count=200,
                                                             include_rts=0,
                                                             exclude_replies=1)
                return response
            return []
        except TwythonAuthError:
            return []
        except TwythonError as te:
            too_many_requests = 429
            if te.error_code != too_many_requests:
                self.logger.error('TwythonError en get_user_timeline:')
                self.logger.error(te)
                self.logger.error(te.error_code)
            if intento < 5:
                self.pause_requests()
                intento += 1
                return self.get_user_timeline(user_screen_name, intento)
            return []

    def get_last_tweets(self, user_screen_name):
        response = self.get_user_timeline(user_screen_name)

        tweets = list()
        for result in response:
            if result['lang'] == 'en': #Los tweets deben estar en ingles porque el lematizador esta analizando ingles
                urls = [url_dict['url'] for url_dict in result['entities']['urls']]
                user_mentions = [mentions_dict['screen_name'] for mentions_dict in result['entities']['user_mentions']]
                tweet = result['text']
                for url in urls:
                    tweet = tweet.replace(url, '')
                for user_mention in user_mentions:
                    tweet = tweet.replace('@%s' % user_mention, '')
                tweet = tweet.lower()
                tweet = self.remove_retweet_from_tweet(tweet)
                tweet = self.remove_remaining_urls(tweet)
                tweet = tweet.strip()
                if tweet not in tweets:
                    tweets.append(tweet)

        return tweets

    def update_status(self, status):
        self.twitterAPI.update_status(status=status)
