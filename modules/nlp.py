from nltk import pos_tag
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.corpus import stopwords
import logging
from applications.RecomendadorExpertosMendeley.modules.propiedades import longitud_promedio_palabras_comunes, \
    numero_palabras_importantes


class Nlp:

    def __init__(self):
        self.lematizador = WordNetLemmatizer()
        self.logger = logging.getLogger('RecomendadorExpertosMendeley.%s' % __name__)

    def get_wordnet_pos(self, treebank_tag):
        if treebank_tag.startswith('J'):
            return wordnet.ADJ
        elif treebank_tag.startswith('V'):
            return wordnet.VERB
        elif treebank_tag.startswith('N'):
            return wordnet.NOUN
        elif treebank_tag.startswith('R'):
            return wordnet.ADV
        else:
            return ''

    def get_pos_tokens(self, frase):
        import re

        # regex adaptado de: https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
        urls = re.compile(r'(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)')
        frase = urls.sub(' ', frase)

        salto_carro = re.compile(r'\\n')
        frase = salto_carro.sub(' ', frase)

        separadores = re.compile(r'[-/]')
        frase = separadores.sub(' ', frase)

        caracteres_no_alfabeticos = re.compile(r'[^a-zA-Z\s]')
        frase = caracteres_no_alfabeticos.sub(' ', frase)

        stop_words = set(stopwords.words('english'))
        palabras = set([palabra.lower() for palabra in frase.split(' ') if len(palabra) > 0])
        tokens = palabras - stop_words
        frase_sin_stopwords = ' '.join(tokens)
        tokens = word_tokenize(frase_sin_stopwords)

        return pos_tag(tokens)

    def get_lemma_pos(self, palabra, pos):
        try:
            lemma = self.lematizador.lemmatize(palabra, pos=self.get_wordnet_pos(pos))
            return lemma
        except KeyError:
            #posible stopword
            return None


    def actualizar_histograma(self, histograma, texto):
        i_palabra = 0
        i_tag = 1

        if histograma is not None:
            pos_tokens = self.get_pos_tokens(texto.lower())
            for pos_token in pos_tokens:
                palabra = pos_token[i_palabra]
                tag = pos_token[i_tag]
                palabra = self.get_lemma_pos(palabra, tag)
                if palabra is not None:
                    if len(palabra) >= longitud_promedio_palabras_comunes:
                        if palabra not in histograma.keys():
                            histograma[palabra] = dict()
                        if tag not in histograma[palabra].keys():
                            histograma[palabra][tag] = 1
                        else:
                            histograma[palabra][tag] += 1


    def crear_histograma_final(self, histograma, numero_minimo_coincidencias=1):
        import operator

        histograma_resultante = dict()
        value_getter = 1

        for palabra in histograma:
            for tag in histograma[palabra]:
                if histograma[palabra][tag] >= numero_minimo_coincidencias:
                    histograma_resultante['%s:%s' % (palabra, tag)] = histograma[palabra][tag]

        return sorted(histograma_resultante.items(), key=operator.itemgetter(value_getter), reverse=True)


    def histogramas_son_similares(self, histograma_tweets, histograma_papers):
        histograma_tweets = self.crear_histograma_final(histograma_tweets, numero_minimo_coincidencias=0)
        histograma_papers = self.crear_histograma_final(histograma_papers, numero_minimo_coincidencias=0)

        tupla_par = 0
        contador_palabras_importantes = 0
        coincidencias = list()
        umbral_palabras_expertos = 1

        for tupla_par_palabra_paper in histograma_papers[:numero_palabras_importantes]:
            for tupla_par_palabra_tweet in histograma_tweets[:numero_palabras_importantes]:
                if tupla_par_palabra_paper[tupla_par] == tupla_par_palabra_tweet[tupla_par]:
                    self.logger.debug('tupla similar: %s = %s' % (tupla_par_palabra_paper[tupla_par],
                                                                  tupla_par_palabra_tweet[tupla_par]))
                    self.logger.debug('histograma_papers[:numero_palabras_importantes]: %s' %
                                      histograma_papers[:numero_palabras_importantes])
                    self.logger.debug('histograma_tweets[:numero_palabras_importantes]: %s' %
                                      histograma_tweets[:numero_palabras_importantes])
                    lemma_coincidente = tupla_par_palabra_paper[tupla_par].split(':')[0]
                    if lemma_coincidente not in coincidencias:
                        coincidencias.append(lemma_coincidente)
                        contador_palabras_importantes += 1

        if contador_palabras_importantes > umbral_palabras_expertos:
            return True
        return False


    def histogramas_son_iguales(self, histograma_tweets, histograma_papers):
        total_palabras_tags_iguales = 0
        umbral_porcentaje_paso = 80

        total_palabras_tag_histograma_papers = 0
        for palabra_paper in histograma_papers:
            total_palabras_tag_histograma_papers += len(histograma_papers[palabra_paper].keys())

        for palabra_tweet in histograma_tweets.keys():
            if palabra_tweet in histograma_papers.keys():
                for tag_palabra_tweet in histograma_tweets[palabra_tweet]:
                    if tag_palabra_tweet in histograma_papers[palabra_tweet]:
                        total_palabras_tags_iguales += 1

        porcentaje_tags_iguales = (total_palabras_tags_iguales * 100.0) / float(total_palabras_tag_histograma_papers)
        if porcentaje_tags_iguales >= umbral_porcentaje_paso:
            return True
        return False
