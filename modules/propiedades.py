db_uri = 'sqlite://database.db'
db_scheduler = 'sqlite://scheduler.db'
# Mendeley Auth
redirection_uri = 'http://127.0.0.1:8000/RecomendadorExpertosMendeley/default/autorizacion'
application_id = 4070
application_secret = 'rjIeMHqQMY0j34Y4'
# Propiedades de la aplicacion
stop_umbral_numero_investigadores = 1000
porcentaje_documentos_a_recorrer = 5
numero_cuentas_expertos_finales = 5
longitud_promedio_palabras_comunes = 6
numero_palabras_importantes = 10
incluir_cuentas_mendeley = False
