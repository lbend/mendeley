from unittest import TestCase
import requests

class testsAPI(TestCase):

    def setUp(self):
        # http://dev.mendeley.com/methods/
        # https://mendeley-show-me-access-tokens.herokuapp.com
        self.access_token = 'MSwxNDkzOTM5NTAxODg2LDQ4NjIxNTM1MSwxMDI4LGFsbCwsLDJiNzM1MDg0NDM1ZGI1MWE5MTRiM2EzLWYwYTcwNmVjYjA4MWMyN2Etc0UsY2Q5NjhmZTAtNzA0Ni0zMDlmLTk5NGItZTY5ZDI4N2RlOTQ5LHJWdkFheURBQjFVNVlWQWFPWEtxU1VTOXNnbw'

    def test_get_datasets(self):
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Content-Type': 'application/vnd.mendeley-public-dataset.1+json'}
        parametros = {'fields': 'results.*',
                      'limit': 500,
                      'type': ['document', 'file_set', 'image', 'raw', 'slides', 'software', 'statistical_data', 'tabular_data', 'video'],
                      # 'type': 'document', #document, file_set, raw, slides, statistical_data, tabular_data
                      'sort': 'popularity',
                      'query': 'bayes perceptron'}
        resultado = requests.get('https://api.mendeley.com/datasets', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text


    def test_get_documents(self):
        '''Parece ser que da solo los documentos que yo tengo en mi cuenta'''
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token}
        parametros = {'include_trashed': True,
                      'view': 'all',
                      'limit': 500,
                      'profile_id': '6911b6b4-6a4c-3397-9565-95a23c2e2423'}
        resultado = requests.get('https://api.mendeley.com/documents', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_documents_by_id(self):
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token}
        parametros = {'view': 'all'}
        document_id = 'f868b81a-9fb0-3d42-9318-267b2ce8e6fe'
        resultado = requests.get('https://api.mendeley.com/documents/%s' % document_id, headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_catalog(self):
        '''Este devolvio full resultados y parece que funciona muy bien!. Ejecutar este primero'''
        parametros = {'limit': 100,
                      'view': 'all',
                      'open_access': True,
                      'query': 'bayes perceptron'}
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        resultado = requests.get('https://api.mendeley.com/catalog', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_catalog_search(self):
        '''Este devolvio full resultados y parece que funciona muy bien!. Ejecutar este primero
        Ademas devuelve ids de documentos junto con nombres de autores'''
        parametros = {'limit': 100,
                      'view': 'all',
                      'open_access': True,
                      'query': 'bayes perceptron'}
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        resultado = requests.get('https://api.mendeley.com/search/catalog', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_document_by_doi(self):
        parametros = {'doi': '10.1162/153244301753683717'}
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        resultado = requests.get('https://api.mendeley.com/catalog', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_catalog_search_basico(self):
        '''Bueno! devuelve con autores'''
        parametros = {'query': 'polar bears',
                      'limit': 3}
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token}
        resultado = requests.get('https://api.mendeley.com/search/catalog', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_metadata(self):
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token}
        parametros = {'title': 'Generalization performance of Bayes optimal classification algorithm for learning a perceptron'}
        resultado = requests.get('https://api.mendeley.com/metadata', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_profiles(self):
        '''Despues de encontrar documentos en el catalogo obtenemos los nombres y mas que nada los ids de los autores
        que consten en mendeley. Ejecutar esto como segundo paso'''
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token}
        parametros = {'authored_document_catalog_id': 'ad005998-d56d-3734-86a9-bd020c8c5a31'}
        resultado = requests.get('https://api.mendeley.com/profiles', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_search_profiles(self):
        '''Muy bien parece que si devuelve los datos del autor como el id buscando por nombres y apellidos
        Elegir solo el primer resultado que salga verificando que coincidan los nombres y apellidos'''
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        parametros = {'query': 'Ralf Herbrich RHERB'}
        resultado = requests.get('https://api.mendeley.com/search/profiles', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_followers(self):
        '''Funciona bien. Ver: http://dev.mendeley.com/methods/#retrieve-the-list-of-followers'''
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        parametros = {'followed': '56c05fee-7598-3292-aeff-884f3e2e250c'}
        resultado = requests.get('https://api.mendeley.com/followers', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_followees(self):
        '''Tambien funciona bien. Ver: http://dev.mendeley.com/methods/#retrieve-the-list-of-followees'''
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        parametros = {'follower': '56c05fee-7598-3292-aeff-884f3e2e250c'}
        resultado = requests.get('https://api.mendeley.com/followers', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_annotations(self):
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Content-Type': 'application/vnd.mendeley-file.1+json'}
        parametros = {'document_id': 'd13502d4-8097-3fe1-9835-363775c5d9d0'}
        resultado = requests.get('https://api.mendeley.com/annotations', headers=cabeceras, params=parametros)
        print resultado.status_code
        print resultado.text

    def test_get_profile_by_id(self):
        cabeceras = {'Authorization': 'Bearer %s' % self.access_token,
                     'Accept': 'application/vnd.mendeley-profiles.1+json'}
        resultado = requests.get('https://api.mendeley.com/profiles/%s' % '56c05fee-7598-3292-aeff-884f3e2e250c',
                                 headers=cabeceras)
        print resultado.status_code
        print resultado.text


# Primero ejecutar test_get_catalog_search, esto nos devuelve los nombres de autores
# despues ejecutar test_search_profiles, esto nos va a devolver el id del profile
# despues podemos ejecutar test_get_followers y test_get_followees