from unittest import TestCase
from applications.RecomendadorExpertosMendeley.modules.GoogleShortenerUrlAPI import GoogleShortenerUrlAPI


class TestsGoogleShortenerUrlAPI(TestCase):

    def testGetUrlCorta(self):
        shortener = GoogleShortenerUrlAPI()
        url_corta = shortener.get_url_corta('https://developers.google.com/url-shortener/v1/getting_started#auth')
        print url_corta