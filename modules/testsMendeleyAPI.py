from unittest import TestCase
import mock
from applications.RecomendadorExpertosMendeley.modules.MendeleyAPI import MendeleyAPI


class TestsMendeleyAPI(TestCase):
    @mock.patch('applications.RecomendadorExpertosMendeley.modules.MendeleyAPI.MendeleyAPI.get_refresh_token',
                return_value='MSw0ODYyMTUzNTEsMTAyOCxhbGwsNWI5NDk0MWM0NTljYjI0MDIzOTk5Y2U4NTVhZTlhYjAxZDM4Z3hycWIsMjUyMi1iNTE4NWNhZWM1MWRiY2E4N2QzLTZhZjRiMzM2NGUxNjNhOGUsMTQ5ODU4ODMwNDE2MCxjZDk2OGZlMC03MDQ2LTMwOWYtOTk0Yi1lNjlkMjg3ZGU5NDksUDRfcmNoY1VSVXNwRllvSVpYZkVzeUVkbTlF')
    def testRenovacionToken(self, mocked_get_refresh_token):
        # https://mendeley-show-me-access-tokens.herokuapp.com

        mendeleyAPI = MendeleyAPI(user_id=1)
        mendeleyAPI.refrescar_token()
