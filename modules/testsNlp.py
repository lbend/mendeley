from unittest import TestCase
from applications.RecomendadorExpertosMendeley.modules.nlp import Nlp


class TestsNlp(TestCase):

    def setUp(self):
        self.nlp = Nlp()


    def test_equidad(self):
        histograma_papers = dict()
        histograma_tweets = dict()

        # abstracts
        self.nlp.actualizar_histograma(histograma_papers, "Source code to be protected, a software application writer's private key, along with an application writer's license provided to the first computer. The application writer's license includes identifying information such as the application writer's name as well as the application writer's public key. A compiler program executed by the first computer compiles the source code into binary code, and computes a message digest for the binary code. The first computer then encrypts the message digest using the application writer's private key, such that the encrypted message digest is defined as a digital 'signature' of the application writer. A software passport is then generated which includes the application writer's digital signature, the application writer's license and the binary code. The software passport is then distributed to a user using any number of software distribution models known in the industry. A user, upon receipt of the software passport, loads the passport into a computer which determines whether the software passport includes the application writer's license and digital signature. In the event that the software passport does not include the application writer's license, or the application writer's digital signature, then the user's computer system discards the software passport and does not execute the binary code. As an additional security step, the user's computer computes a second message digest for the software passport and compares it to the first message digest, such that if the first and second message digests are not equal, the software passport is also rejected by the user's computer and the code is not executed. If the first and second message digests are equal, the user's computer extracts the application writer's public key from the application writer's license for verification. The application writer's digital signature is decrypted using the application writer's public key. The user's computer then compares a message digest of the binary code to be executed, with the decrypted application writer's digital signature, such that if they are equal, the user's computer executes the binary code.")
        self.nlp.actualizar_histograma(histograma_papers, "A formal approach to security in the software life cycle is essential to protect corporate resources. However, little thought has been given to this aspect of software development. Traditionally, software security has been treated as an afterthought leading to a cycle of 'penetrate and patch.' Due to its criticality, security should be integrated as a formal approach .in the software life cycle. Both a software security checklist and assessment tools should be incorporated into this life cycle process. The current research at JPL addresses both of these areas through the development of a Software Security Assessment Instrument (SSAI). This paper focuses on the development of a Software Security Checklist (SSC) for the life cycle. It includes the critical areas of requirements gathering and specijication, design and code issues, and maintenance and decommissioning of software and systems.")
        self.nlp.actualizar_histograma(histograma_papers, "We study the effect of user incentives on software security in a network of individual users under costly patching and negative network security externalities. For proprietary software or freeware, we compare four alternative policies to manage network security: (i) consumer self-patching (where no external incentives are provided for patching or purchasing); (ii) mandatory patching; (iii) patching rebate; and (iv) usage tax. We show that for proprietary software, when the software security risk and the patching costs are high, for both a welfare-maximizing social planner and a profit-maximizing vendor, a patching rebate dominates the other policies. However, when the patching cost or the security risk is low, self-patching is best. We also show that when a rebate is effective, the profit-maximizing rebate is decreasing in the security risk and increasing in patching costs. The welfare-maximizing rebates are also increasing in patching costs, but can be increasing in the effective security risk when patching costs are high. For freeware, a usage tax is the most effective policy except when both patching costs, and security risk are low, in which case a patching rebate prevails. Optimal patching rebates and taxes tend to increase with increased security risk and patching costs, but can decrease in the security risk for high-risk levels. Our results suggest that both the value generated from software and vendor profits can be significantly improved by mechanisms that target user incentives to maintain software security.")

        # tweets
        # self.nlp.actualizar_histograma(histograma_tweets, "Micropatching CVE-2017-0290 (Windows Malware Protection Engine RCE) https://0patch.blogspot.fr/2017/05/0patching-worst-windows-remote-code.html #vulnerability #patching")
        # self.nlp.actualizar_histograma(histograma_tweets, "Another fake #WannaCry. Doesn't encrypt. Has a filename of MS17-010.exe. Indonesian text? https://www.virustotal.com/en/file/a4704be3a77f989693188a4a505b62719ffe87718f8891ab5d3e1de1b1a57572/analysis/ @malwrhunterteam")
        # self.nlp.actualizar_histograma(histograma_tweets, "NETATTACK 2 - An Advanced Wireless Network Scan and Attack Script Based on GUI https://goo.gl/2O1efV  #Attack #Linux #netattack")

        # POTUS tweets
        self.nlp.actualizar_histograma(histograma_tweets,
                                       "Happy Mother's Day!")
        self.nlp.actualizar_histograma(histograma_tweets,
                                       "50,000+ react to @POTUS @realDonaldTrump on the jumbotron this morning at @LibertyU. Congratulations to the Class of 2017!! @JerryFalwellJr")
        self.nlp.actualizar_histograma(histograma_tweets,
                                       "Happy Mother's Day to all moms, especially to my own mom & to my wife Karen, who's been an incredible mother to our 3 kids. Thank you all!")
        self.nlp.actualizar_histograma(histograma_tweets, "Dems have been complaining for months & months about Dir. Comey. Now that he has been fired they PRETEND to be aggrieved. Phony hypocrites!")
        self.nlp.actualizar_histograma(histograma_tweets, "Cryin' Chuck Schumer stated recently, 'I do not have confidence in him (James Comey) any longer.' Then acts so indignant.  #draintheswamp")

        print self.nlp.histogramas_son_similares(histograma_tweets, histograma_papers)

    def test_crear_histograma_final(self):
        import cPickle

        with open('../modules/pickles/histograma_papers.pickle', 'rb') as histograma_papers_pickle:
            histograma_papers = cPickle.load(histograma_papers_pickle)
            histograma_papers = self.nlp.crear_histograma_final(histograma_papers)

            # TODO falta terminar
