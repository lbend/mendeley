# coding=utf-8
from unittest import TestCase
import mock
from SchedulerTasksTopico import SchedulerTasksTopico
import logging
from gluon import current


class TestScheduler(TestCase):

    def setUp(self):
        # logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger('RecomendadorExpertosMendeley')
        self.logger.setLevel(logging.DEBUG)
        handler = logging.FileHandler('TestsScheduler.log')
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    @mock.patch('applications.RecomendadorExpertosMendeley.modules.MendeleyAPI.MendeleyAPI.get_access_token',
                return_value='MSwxNDk4MzIyMDgxOTc3LDQ4NjIxNTM1MSwxMDI4LGFsbCwsLGY4NjYtMDI1ZWZhYWRjNTFmZWViNWE2NGE5NjVhNjZmODY1OTc1MTgsY2Q5NjhmZTAtNzA0Ni0zMDlmLTk5NGItZTY5ZDI4N2RlOTQ5LEZxblRCOExaR3Y1Nll0SFNQQzRHV3VLazR5OA')
    def test_procesar_topico(self, mocked_get_access_token):
        # https://mendeley-show-me-access-tokens.herokuapp.com

        self.logger.debug('Prueba')
        nombre_topico = 'Prueba'
        lista_querys_test = ['ergonomics laparoscopic surgery']
        scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test, user_id=1)
        status_twitter, status_mendeley = scheduler_tasks.procesar_topico()
        self.logger.debug('Status mendeley: %s' % status_mendeley)
        self.logger.debug('Status twitter: %s' % status_twitter)

        # self.logger.debug('Perceptron')
        # nombre_topico = 'Perceptron'
        # lista_querys_test = ['bayes perceptron']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test, user_id=1)
        # status_twitter, status_mendeley = scheduler_tasks.procesar_topico()
        # self.logger.debug('Status mendeley: %s' % status_mendeley)
        # self.logger.debug('Status twitter: %s' % status_twitter)

        # print '%s: %s' % ('Para Perceptron', scheduler_tasks.procesar_topico())

        # Perceptron: Te recomendamos seguir a los usuarios @rherbrich @ColinCampbellx @julianohifi @Xandi_Nino @FrankDelaney @sanjoydasgupta1 @yogoldsmith @TaylorJTakeover @SultanofStat @kmichelle @Domo1016 @yuer922 @stephenclark @yuer922 @stephenclark @AdamMargolin @ONGShaoen @Monica_Schenone @eylesandgould @StevieCarr66 @kmichelle @NadiaNedjah @jbuenogarcia @waltercarnielli @ryanmartin @bayacsm @eylesandgould @ONGShaoen @Monica_Schenone @XuanZhuuu @ONGShaoen @Monica_Schenone @eylesandgould @S_Morris17 @ritabandutta @goyalanshul @LotfiEhsan @luisvarona @cubanbrothers @DJPaulyD @LeRoyBell @ChangpingLi2 @JunChongMA @CuiZhuang @zhangcui314 @MassJwJ @MassJwJ @Maria @M_AliAslam @YadongW @SeonghoTbaki @annielangg @GuohuaWang2 @yunlongliu @andrycorp @LGSrock101 @LuigiBOCCIA4 @IvanLuJianWen @ShuyanChen @nocci81 @rbellazzi @stuart_aitken @mayuri_pandya @AlokSharma_RDG @AM_Fanatics

        # print 'cardiac arrest'
        # nombre_topico = 'RCP'
        # lista_querys_test = ['cardiac arrest']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # status_twitter, status_mendeley = scheduler_tasks.procesar_topico()
        # self.logger.debug('Status mendeley: %s' % status_mendeley)
        # self.logger.debug('Status twitter: %s' % status_twitter)

        # print '%s: %s' % ('Para RCP', scheduler_tasks.procesar_topico())

        # print 'cardiovascular life support'
        # nombre_topico = 'RCP'
        # lista_querys_test = ['cardiovascular life support']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # status_twitter, status_mendeley = scheduler_tasks.procesar_topico()
        # self.logger.debug('Status mendeley: %s' % status_mendeley)
        # self.logger.debug('Status twitter: %s' % status_twitter)

        # print '%s: %s' % ('Para RCP', scheduler_tasks.procesar_topico())

    @mock.patch('applications.RecomendadorExpertosMendeley.modules.MendeleyAPI.MendeleyAPI.get_access_token',
                return_value='MSwxNDk3OTczMjUxNTU0LDQ4NjIxNTM1MSwxMDI4LGFsbCwsLDQ4MzU1MTE1ZmI1Y2M1MTY3MzFmNjMxYWVkYjU0ODgxYTFjYjUzNyxjZDk2OGZlMC03MDQ2LTMwOWYtOTk0Yi1lNjlkMjg3ZGU5NDksNnM2LV9saE5DVW5rdV9COWo0OXl2MDA3cldF')
    def test_get_cuentas_expertos_finales(self, mocked_get_access_token):
        import cPickle

        # https://mendeley-show-me-access-tokens.herokuapp.com

        # self.logger.debug('Perceptron')
        # nombre_topico = 'Perceptron'
        # lista_querys_test = ['bayes perceptron']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # with open('../modules/pickles/histograma_bayes_perceptron.pickle', 'rb') as histograma_bayes_perceptron_pickle:
        #     scheduler_tasks.histograma_topico = cPickle.load(histograma_bayes_perceptron_pickle)
        # with open('../modules/pickles/grafo_bayes_perceptron.pickle', 'rb') as grafo_bayes_perceptron_pickle:
        #     grafo_investigadores = cPickle.load(grafo_bayes_perceptron_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])
        # self.logger.debug('Cuentas de expertos finales:')
        # self.logger.debug(cuentas_expertos_finales)

        # self.logger.debug('RCP')
        # nombre_topico = 'RCP'
        # lista_querys_test = ['cardiac arrest']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # with open('../modules/pickles/histograma_cardiac_arrest.pickle', 'rb') as histograma_cardiac_arrest_pickle:
        #     scheduler_tasks.histograma_topico = cPickle.load(histograma_cardiac_arrest_pickle)
        # with open('../modules/pickles/grafo_cardiac_arrest.pickle', 'rb') as grafo_cardiac_arrest_pickle:
        #     grafo_investigadores = cPickle.load(grafo_cardiac_arrest_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])
        # self.logger.debug('Cuentas de expertos finales:')
        # self.logger.debug(cuentas_expertos_finales)

        # self.logger.debug('RCP')
        # nombre_topico = 'RCP'
        # lista_querys_test = ['cardiovascular life support']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # with open('../modules/pickles/histograma_cardiovascular_life_support.pickle', 'rb') as histograma_cardiovascular_life_support_pickle:
        #     scheduler_tasks.histograma_topico = cPickle.load(histograma_cardiovascular_life_support_pickle)
        # with open('../modules/pickles/grafo_cardiovascular_life_support.pickle', 'rb') as grafo_cardiovascular_life_support_pickle:
        #     grafo_investigadores = cPickle.load(grafo_cardiovascular_life_support_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])

        # 2017-06-01 16:54:00,541 - mendeley.applications.mendeley.modules.SchedulerTasksTopico - DEBUG - Experto @RMTPhDJD encontrado en base al nombre de experto Ryan Tanner.
        # {"id":"c3a2f046-060b-32e3-bebb-e145493d2e36","first_name":"Ryan","last_name":"Tanner","display_name":"Ryan Tanner","link":"https://www.mendeley.com/profiles/ryan-tanner/","folder":"ryan-tanner","research_interests_list":[],"academic_status":"Student  > Ph. D. Student","discipline":{"name":"Engineering","subdisciplines":[]},"disciplines":[{"name":"Engineering","subdisciplines":[]}],"photo":{"standard":"https://photos.mendeley.com/awaiting/120/120/Unlhbg/VGFubmVy","square":"https://photos.mendeley.com/awaiting/48/48/Unlhbg/VGFubmVy"},"photos":[{"width":48,"height":48,"url":"https://photos.mendeley.com/awaiting/48/48/Unlhbg/VGFubmVy","original":false},{"height":120,"url":"https://photos.mendeley.com/awaiting/120/120/Unlhbg/VGFubmVy","original":false},{"width":64,"height":64,"url":"https://photos.mendeley.com/awaiting/64/64/Unlhbg/VGFubmVy","original":false},{"width":128,"height":128,"url":"https://photos.mendeley.com/awaiting/128/128/Unlhbg/VGFubmVy","original":false},{"width":256,"height":256,"url":"https://photos.mendeley.com/awaiting/256/256/Unlhbg/VGFubmVy","original":false}],"verified":true,"user_type":"normal","location":{"id":"d4a0288f-39d2-45ec-8c31-35ee6b8ba4e8","latitude":40.0468,"longitude":-105.213,"name":"Boulder, Colorado, United States","city":"Boulder","state":"Colorado","country":"United States"},"created":"2010-10-21T23:01:57.000Z","prefill_draft":false,"visibility":"all","scopus_author_ids":[],"privacy_restricted_view":false}

        # 10% de documentos:

        # self.logger.debug('Perceptron')
        # nombre_topico = 'Perceptron'
        # lista_querys_test = ['bayes perceptron']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # with open('../modules/pickles/histograma_bayes_perceptron_10percent.pickle', 'rb') as histograma_bayes_perceptron_pickle:
        #     scheduler_tasks.histograma_topico = cPickle.load(histograma_bayes_perceptron_pickle)
        # with open('../modules/pickles/grafo_bayes_perceptron_10percent.pickle', 'rb') as grafo_bayes_perceptron_pickle:
        #     grafo_investigadores = cPickle.load(grafo_bayes_perceptron_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])
        # (@vivian1ee), @peterajohnson, (@li_shuda), @Quenestil, @lemire, [@AshleyFlip], @HerrFilter, @jeridfrancom, @williamsurles, @bdobrica, @ricckli, @SaraSaperstein, [@Amit03sk], @inertcatnip, @frankvvang, @josephkelly, @OzgeYeloglu


        # mayores a 4-gramas

        # self.logger.debug('Perceptron')
        # nombre_topico = 'Perceptron'
        # lista_querys_test = ['bayes perceptron']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # with open('../modules/pickles/histograma_perceptron_4gram.pickle', 'rb') as histograma_perceptron_4gram_pickle:
        #     scheduler_tasks.histograma_topico = cPickle.load(histograma_perceptron_4gram_pickle)
        # with open('../modules/pickles/grafo_perceptron_4gram.pickle', 'rb') as grafo_perceptron_4gram_pickle:
        #     grafo_investigadores = cPickle.load(grafo_perceptron_4gram_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])

        # Mayores a 4-gramas e histogramas ahora utilizan informacion de longitud de lemmas en lugar de cantidad de coincidencias de lemmas

        # self.logger.debug('Perceptron')
        # nombre_topico = 'Perceptron'
        # lista_querys_test = ['bayes perceptron']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # with open('../modules/pickles/histograma_perceptron_4gram_len_lemma.pickle', 'rb') as histograma_perceptron_4gram_len_lemma_pickle:
        #     scheduler_tasks.histograma_topico = cPickle.load(histograma_perceptron_4gram_len_lemma_pickle)
        # with open('../modules/pickles/grafo_perceptron_4gram_len_lemma.pickle', 'rb') as grafo_perceptron_4gram_len_lemma_pickle:
        #     grafo_investigadores = cPickle.load(grafo_perceptron_4gram_len_lemma_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])

        # Mayores o iguales a 4-gramas e histogramas ahora utilizan informacion de longitud de lemmas en lugar de cantidad de coincidencias de lemmas

        # self.logger.debug('Perceptron')
        # nombre_topico = 'Perceptron'
        # lista_querys_test = ['bayes perceptron']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # with open('../modules/pickles/histograma_perceptron_mayor_igual_4gram_len_lemma.pickle',
        #           'rb') as histograma_perceptron_mayor_igual_4gram_len_lemma_pickle:
        #     scheduler_tasks.histograma_topico = cPickle.load(histograma_perceptron_mayor_igual_4gram_len_lemma_pickle)
        # with open('../modules/pickles/grafo_perceptron_mayor_igual_4gram_len_lemma.pickle',
        #           'rb') as grafo_perceptron_mayor_igual_4gram_len_lemma_pickle:
        #     grafo_investigadores = cPickle.load(grafo_perceptron_mayor_igual_4gram_len_lemma_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])

        # Mayores o iguales a 3-gramas e histogramas ahora utilizan informacion de longitud de lemmas en lugar de cantidad de coincidencias de lemmas. >= 0
        # len gramma: 3
        # >= numero_minimo_coincidencias
        # contador_palabras_importantes > 0

        # self.logger.debug('Perceptron')
        # nombre_topico = 'Perceptron'
        # lista_querys_test = ['bayes perceptron']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test)
        # with open('../modules/pickles/histograma_perceptron_longitud_lemma_3_mayor_igual_0.pickle',
        #           'rb') as histograma_perceptron_longitud_lemma_3_mayor_igual_0_pickle:
        #     scheduler_tasks.histograma_topico = cPickle.load(histograma_perceptron_longitud_lemma_3_mayor_igual_0_pickle)
        # with open('../modules/pickles/grafo_perceptron_longitud_lemma_3_mayor_igual_0.pickle',
        #           'rb') as grafo_perceptron_longitud_lemma_3_mayor_igual_0_pickle:
        #     grafo_investigadores = cPickle.load(grafo_perceptron_longitud_lemma_3_mayor_igual_0_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])

        # Funciono bien, ver el archivo CEDIA/RedesSocialesExpertos/ParametrosExpertosEncontrados de googledocs
        self.logger.debug('Perceptron')
        nombre_topico = 'Perceptron'
        lista_querys_test = ['bayes perceptron']
        scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test, user_id=1)
        with open('../modules/pickles/documentos_perceptron.pickle', 'rb') as documentos_perceptron:
            documentos = cPickle.load(documentos_perceptron)
            scheduler_tasks.actualizar_histograma_documentos(documentos)
        with open('../modules/pickles/grafo_perceptron_longitud_lemma_3_mayor_igual_0.pickle',
                  'rb') as grafo_perceptron_longitud_lemma_3_mayor_igual_0_pickle:
            grafo_investigadores = cPickle.load(grafo_perceptron_longitud_lemma_3_mayor_igual_0_pickle)
            ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        self.logger.debug('Cuentas de expertos finales:')
        for cuenta in cuentas_expertos_finales.keys():
            if len(cuentas_expertos_finales[cuenta]) >= 1:
                self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        for cuenta in cuentas_expertos_finales.keys():
            if len(cuentas_expertos_finales[cuenta]) == 2:
                self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])

        # self.logger.debug('cardiovascular life support')
        # nombre_topico = 'RCP'
        # lista_querys_test = ['cardiovascular life support']
        # scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test, user_id=1)
        # with open('../modules/pickles/documentos_cardiovascular_life_support.pickle', 'rb') as documentos_cardiovascular_life_support:
        #     documentos = cPickle.load(documentos_cardiovascular_life_support)
        #     scheduler_tasks.actualizar_histograma_documentos(documentos)
        # with open('../modules/pickles/grafo_cardiovascular_life_support.pickle',
        #           'rb') as grafo_cardiovascular_life_support_pickle:
        #     grafo_investigadores = cPickle.load(grafo_cardiovascular_life_support_pickle)
        #     ids_expertos_finales = scheduler_tasks.get_ids_expertos_finales(grafo_investigadores)
        # cuentas_expertos_finales = scheduler_tasks.get_cuentas_expertos_finales(ids_expertos_finales)
        # self.logger.debug('Cuentas de expertos finales:')
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) >= 1:
        #         self.logger.debug('expertos mendeley: %s' % cuentas_expertos_finales[cuenta][0])
        # for cuenta in cuentas_expertos_finales.keys():
        #     if len(cuentas_expertos_finales[cuenta]) == 2:
        #         self.logger.debug('expertos twitter: %s' % cuentas_expertos_finales[cuenta][1])

    def test_actualizar_histograma_documentos(self):
        import cPickle

        self.logger.debug('Perceptron')
        nombre_topico = 'Perceptron'
        lista_querys_test = ['bayes perceptron']
        scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test, user_id=1)
        with open('../modules/pickles/documentos_perceptron.pickle', 'rb') as documentos_perceptron:
            documentos = cPickle.load(documentos_perceptron)
            scheduler_tasks.actualizar_histograma_documentos(documentos)

    def test_construir_status_twitter(self):
        import cPickle

        self.logger.debug('Perceptron')
        nombre_topico = 'Perceptron'
        lista_querys_test = ['bayes perceptron']
        scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test, user_id=1)
        with open('../modules/pickles/cuentas_expertos_finales_perceptron.pickle', 'rb') as cuentas_expertos_finales_perceptron:
            cuentas_expertos_finales = cPickle.load(cuentas_expertos_finales_perceptron)
            status_twitter, status_mendeley = scheduler_tasks.construir_status_twitter(cuentas_expertos_finales)
            self.logger.debug('Status mendeley: %s' % status_mendeley)
            self.logger.debug('Status twitter: %s' % status_twitter)

        # Pruebo con nombres de topico que tengan tildes u otros caracteres
        self.logger.debug('Perceptron')
        nombre_topico = 'Perceptrón'
        lista_querys_test = ['bayes perceptron']
        scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test, user_id=1)
        with open('../modules/pickles/cuentas_expertos_finales_perceptron.pickle',
                  'rb') as cuentas_expertos_finales_perceptron:
            cuentas_expertos_finales = cPickle.load(cuentas_expertos_finales_perceptron)
            status_twitter, status_mendeley = scheduler_tasks.construir_status_twitter(cuentas_expertos_finales)
            self.logger.debug('Status mendeley: %s' % status_mendeley)
            self.logger.debug('Status twitter: %s' % status_twitter)

    def testProcesarExpertosAPublicar(self):
        self.logger.debug('Perceptron')
        nombre_topico = 'Perceptron'
        lista_querys_test = ['bayes perceptron']
        scheduler_tasks = SchedulerTasksTopico(nombre_topico, lista_querys_test, user_id=1)

        status_mendeley = '#Perceptron: En Mendeley te recomendamos seguir a los usuarios https://www.mendeley.com/profiles/sujatha-ramdorai/ https://www.mendeley.com/profiles/rajnish-dhiman/ https://www.mendeley.com/profiles/ulrich-schuermann/ https://www.mendeley.com/profiles/philip-yeo/ https://www.mendeley.com/profiles/ali-caglayan/ https://www.mendeley.com/profiles/matt-cecchini/ https://www.mendeley.com/profiles/ainsley-close/ https://www.mendeley.com/profiles/jeff-berry/ https://www.mendeley.com/profiles/jiawen-xie/ https://www.mendeley.com/profiles/william-daniell/ https://www.mendeley.com/profiles/jonathan-childers/ https://www.mendeley.com/profiles/erin-walline/ https://www.mendeley.com/profiles/edison-da-silva/ https://www.mendeley.com/profiles/ahmet-dikici/ https://www.mendeley.com/profiles/renato-camata/ https://www.mendeley.com/profiles/vassili-demergis/ https://www.mendeley.com/profiles/philip-flip-kromer/ https://www.mendeley.com/profiles/ricardo-gasteazoro/ https://www.mendeley.com/profiles/huckelberry-finne/ https://www.mendeley.com/profiles/tasneem-kaochar/ https://www.mendeley.com/profiles/denisse-pascual/ https://www.mendeley.com/profiles/evangelos-milios/ https://www.mendeley.com/profiles/andrew-carpenter/ https://www.mendeley.com/profiles/ozge-yeloglu/ https://www.mendeley.com/profiles/royal-frasier/ https://www.mendeley.com/profiles/jonathan-blaine/ https://www.mendeley.com/profiles/andrea-salvini/ https://www.mendeley.com/profiles/cynthia-purvis/ https://www.mendeley.com/profiles/peter-hartmund-jorgensen/ https://www.mendeley.com/profiles/e-alan-hogue/ https://www.mendeley.com/profiles/lubos-omelina/ https://www.mendeley.com/profiles/elina-yli-rantala/ https://www.mendeley.com/profiles/tobias-bartsch/ https://www.mendeley.com/profiles/frederik-geth/ https://www.mendeley.com/profiles/ricardo-rodrigues2/ https://www.mendeley.com/profiles/paulo-duarte/ https://www.mendeley.com/profiles/william-surles/ https://www.mendeley.com/profiles/sridhar-padmanabhan/ https://www.mendeley.com/profiles/mousumi-rahman/ https://www.mendeley.com/profiles/robert-filter/ https://www.mendeley.com/profiles/muhammet-kasim-bicakci/ https://www.mendeley.com/profiles/alireza-b-parsa/ https://www.mendeley.com/profiles/peter-may-ostendorp/ https://www.mendeley.com/profiles/gert-kruger/ https://www.mendeley.com/profiles/jessica-may/ https://www.mendeley.com/profiles/sarah-schneider/ https://www.mendeley.com/profiles/tahir-bicakci/ https://www.mendeley.com/profiles/ryan-tanner/ https://www.mendeley.com/profiles/federico-buti/ https://www.mendeley.com/profiles/tilo-peter/ https://www.mendeley.com/profiles/andrew-shantz1/ https://www.mendeley.com/profiles/eva-kucich/ https://www.mendeley.com/profiles/monica-sbrana/ https://www.mendeley.com/profiles/jari-keskinen/ https://www.mendeley.com/profiles/sukru-tikves/ https://www.mendeley.com/profiles/bodo-henkel/ https://www.mendeley.com/profiles/serban-nicolae-stamatin/ https://www.mendeley.com/profiles/sune-veltze/ https://www.mendeley.com/profiles/arne-erdmann/ https://www.mendeley.com/profiles/eray-tuzun/ https://www.mendeley.com/profiles/jose-m-soler/ https://www.mendeley.com/profiles/neal-kruis/ https://www.mendeley.com/profiles/jan-mazanec/ https://www.mendeley.com/profiles/john-kenworthy/ https://www.mendeley.com/profiles/yeming-hu/ https://www.mendeley.com/profiles/alexandra-aguirre-rodriguez1/ https://www.mendeley.com/profiles/chantal-kabus/ https://www.mendeley.com/profiles/mike-hammond/ https://www.mendeley.com/profiles/amber-lenhart/ https://www.mendeley.com/profiles/adrian-frick/ https://www.mendeley.com/profiles/edgard-amorim1/ https://www.mendeley.com/profiles/michael-bartha/ https://www.mendeley.com/profiles/isabel-keller/ https://www.mendeley.com/profiles/johan-akerman/ https://www.mendeley.com/profiles/alicia-arredondo/ https://www.mendeley.com/profiles/colin-campbell14/ https://www.mendeley.com/profiles/matthew-rhoades/ https://www.mendeley.com/profiles/tobias-kienzler/ https://www.mendeley.com/profiles/francis-forte/ https://www.mendeley.com/profiles/babak-dastmalchi/ https://www.mendeley.com/profiles/vikas-bagri/ https://www.mendeley.com/profiles/vinothini-venkatachalam/ https://www.mendeley.com/profiles/suzanne-amaro/ https://www.mendeley.com/profiles/linda-sandblad/ https://www.mendeley.com/profiles/joseph-kelly/ https://www.mendeley.com/profiles/gregor-henze/ https://www.mendeley.com/profiles/aydin-alatan/ https://www.mendeley.com/profiles/fatih-gulec/ https://www.mendeley.com/profiles/koen-thijs/ https://www.mendeley.com/profiles/eric-wilson/ https://www.mendeley.com/profiles/oguzhan-guclu/ https://www.mendeley.com/profiles/nivaldo-peroni/ https://www.mendeley.com/profiles/oliver-grimm/ https://www.mendeley.com/profiles/martin-kochanczyk/ https://www.mendeley.com/profiles/tianyi-wan/ https://www.mendeley.com/profiles/ayca-tarhan/ https://www.mendeley.com/profiles/lakshmi-raju/ https://www.mendeley.com/profiles/rodrigo-oeiras/ https://www.mendeley.com/profiles/stephen-larson/ https://www.mendeley.com/profiles/dimitris-iakovidis1/ https://www.mendeley.com/profiles/erin-hughes1/ https://www.mendeley.com/profiles/prathamesh-kulkarni1/ https://www.mendeley.com/profiles/viktor-hrkac/ https://www.mendeley.com/profiles/matthias-falkner/ https://www.mendeley.com/profiles/levent-seckin/ https://www.mendeley.com/profiles/andrew-carnie/ https://www.mendeley.com/profiles/maximilian-held/ https://www.mendeley.com/profiles/bogdan-dobrica/ https://www.mendeley.com/profiles/sandra-vogel/ https://www.mendeley.com/profiles/venkata-sai-kiran-chakravadhanula/ https://www.mendeley.com/profiles/stamatin-ioan/ https://www.mendeley.com/profiles/oguz-aslanturk/ https://www.mendeley.com/profiles/morten-eskildsen/ https://www.mendeley.com/profiles/june-spector/ https://www.mendeley.com/profiles/zachary-lindsey/ https://www.mendeley.com/profiles/john-rice/ https://www.mendeley.com/profiles/rajendra-yadav/'
        status_mendeley_final = '#Perceptron: En Mendeley te recomendamos seguir a los usuarios https://www.mendeley.com/profiles/sujatha-ramdorai/'
        status_mendeley_procesado = scheduler_tasks.procesar_expertos_a_publicar(status=status_mendeley)
        assert status_mendeley_procesado == status_mendeley_procesado

        status_twitter = '#Perceptron: En Twitter te recomendamos seguir a los usuarios @hjelmj @aspilos74 @caamitkulkarni @peterajohnson @alanyliu'
        status_twitter_procesado = scheduler_tasks.procesar_expertos_a_publicar(status=status_twitter)
        assert status_twitter_procesado == status_twitter
